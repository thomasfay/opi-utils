fpath=$1
if (( $# < 1 )); then
    echo "usage: $0 <filepath>"
    exit
fi

for topScreen in `ls $PWD/$fpath | grep bob`
do
    a=$(grep '.bob</file>' $topScreen)
    a=${a//<file>/}
    a=${a//<\/file>/}
    for linkedFile in $a
    do
        res=$(eval file "$PWD/$linkedFile" | grep XML)
        [ $? -eq 0 ] || echo "XML File does not exist: $linkedFile - required by $topScreen"
    done
    a=$(grep '.png</file>' $topScreen)
    a=${a//<file>/}
    a=${a//<\/file>/}
    # Handle leading spaces/spaces in file name
    a=${a//    /}
    a=${a// /&}
    for linkedFile in $a
    do
        linkedFile=${linkedFile//&/ }
        res=$(file "$PWD/$linkedFile" | grep PNG)
        [ $? -eq 0 ] || echo "PNG File does not exist: $linkedFile - required by $topScreen"
    done 
done
