#!/usr/bin/env python3
"""
Fetches ESS FBS from available JSON url
Unix style and GNU style options are valid.
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('inFile', help = 'Input OPI file.')
parser.add_argument('tag', help='Tag with which to check termination.')
parser.add_argument('--start', type=int, help='Start checks from line n.')

args = parser.parse_args()
inFile = args.inFile
tag = args.tag
start = args.start

if start is None:
    start = 0

with open(inFile, 'r') as infile:
    listLines = infile.readlines()

listLines = listLines[start:]
nLines = len(listLines)

match = '<' + tag
term = '</' + tag + '>'
number=''
endNumber=''

fMatches=0
rMatches=0

print("*" * 10 + "Checking for '" + tag + "' in sequence...")
# Check all opens are terminated
for n,line in enumerate(listLines):
    if match in line:
        # Exclude similar tags that could match
        if not (line[line.find(match) + len(match)].isalpha()):
            # Exit in single line tag
            if term in line:
                print("'" + tag + "' is a single line tag, exiting.")
                exit()
            # Get indent also
            cols = str(len(line) - len(line.lstrip(' ')))
            number = str(start + n+1)
            for i in range(n+2,nLines):
                if term in listLines[i]:
                    endLine = listLines[i]
                    endCols = str(len(endLine) - len(endLine.lstrip(' ')))
                    if endCols == cols:
                        endNumber = str(start + i+1)
                        fMatches += 1
                        break
            if i == nLines - 1:
                endNumber = 'invalid'
            print("%s->%s, %s" % (number,endNumber, cols)) 

print("*" * 10 + "Checking for '" + tag + "' in reverse sequence...")
# Check all closes have opens
reversedLines = listLines
reversedLines.reverse()
number=''
cols=''
endCols=''
for n,line in enumerate(reversedLines):
    if term in line:
        endNumber = start + nLines - n
        endCols = str(len(line) - len(line.lstrip(' ')))
        for i in range(n,nLines):
            startLine = reversedLines[i]
            if match in startLine:
                cols = str(len(startLine) - len(startLine.lstrip(' ')))
                if cols == endCols:
                    number = start + nLines - i
                    rMatches += 1
                    break
        if i == nLines - 1:
            number = 'invalid'
        print("%s->%s, %s" % (endNumber, number, endCols)) 
print("*" * 10 + "%s forward completions; %s backward completions" % (fMatches,rMatches))
