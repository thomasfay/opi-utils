#!/usr/bin/env python3
"""
Fetches ESS FBS from available JSON url
Unix style and GNU style options are valid.
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('inFile', help ='Output file for the resultant breakdown listing in JSON format.')
parser.add_argument('--show', nargs ='?', const = True, help='Show line.')
parser.add_argument('--start', type=int, help='Start looking from line n. Can work together with lines argument.')
parser.add_argument('--lines', type=int, help='Break after first n line.')

args = parser.parse_args()
inFile = args.inFile
show = args.show
nLines = args.lines
start = args.start

with open(inFile, 'r') as infile:
    listLines = infile.readlines()

if start is not None:
    if start <= 0:
        start = 1
    listLines = listLines[start-1:]
else:
    start = 1

disp = ''
for linenumber,line in enumerate(listLines):
    if nLines is not None:
        if linenumber == nLines:
            break
    nSpace = 0
    for char in line:
        if char.isspace():
            nSpace += 1
        else:
            if show is not None:
                disp = " " + line[nSpace:].rstrip()
            print("%s: %s%s" % (start + linenumber, nSpace, disp)) 
            break
