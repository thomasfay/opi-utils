fname=$1
if (( $# < 1 )); then
    echo "usage: $0 <filename>"
    exit
fi

a=$(grep '.bob</file>' $fname)
a=${a//<file>/}
a=${a//<\/file>/}
for linkedFile in $a
do
    echo $linkedFile
done
