#!/usr/bin/env python3
"""
Fetches ESS FBS from available JSON url
Unix style and GNU style options are valid.
"""
import argparse

parser = argparse.ArgumentParser()
parser.add_argument('inFile', help = 'Input OPI file.')
parser.add_argument('--line', nargs ='?', const = True, help='Show line number of widget.')

args = parser.parse_args()
inFile = args.inFile
line = args.line

with open(inFile, 'r') as infile:
    listLines = infile.readlines()

matchWidget = '<widget type='
number=''
for n,line in enumerate(listLines):
    if matchWidget in line:
        widget = line.lstrip().replace(matchWidget,'')
        widget = widget[:-2]
        name = listLines[n+1].lstrip().rstrip()
        name = name.replace('<name>','').replace('</name>','')
        if line:
            number = str(n) + ': '
        print("%s%s (%s)" % (number,name, widget)) 

