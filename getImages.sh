fname=$1
if (( $# < 1 )); then
    echo "usage: $0 <filename>"
    exit
fi

a=$(grep '.png</file>' $fname)
a=${a//<file>/}
a=${a//<\/file>/}
a=${a//    /}
a=${a// /&}
for linkedFile in $a
do
    linkedFile=${linkedFile//&/ }
    echo $linkedFile
done
a=$(grep '.png</value>' $fname)
a=${a//<value>/}
a=${a//<\/value>/}
a=${a//    /}
a=${a// /&}
for linkedFile in $a
do
    linkedFile=${linkedFile//&/ }
    echo $linkedFile
done
